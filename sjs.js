/**
* SunnyJs a samll js library
* @lisence GNU:GPL
* @author gaoyang  <yunnysunny@gmail.com>
* @date 2012.3.18
*/
(function () {
	var packageName = 'SJS';
	if (!window[packageName]) {
		window[packageName] = {};
	}
	function isCompatible(other) {
		if (other == false
			|| !Array.property.push
			|| !Object.hasOwnProperty
			|| !document.createElement
			|| !document.getElementsByTagName) {
			return false;
		}
		else {
			return true;
		}
	}
	window[packageName]['isCompatible'] = isCompatible;

	function $() {
		var elements = new Array();

		for (var i = 0; i < arguments.length; i++) {
			var element = arguments[i];

			if (typeof element == 'string') {
				element = document.getElementById(element);
			}
			if (arguments.length == 1) {
				return element;
			}

			elements.push(element);
		}
		return elements;
	}

	window[packageName]['$'] = $;

	function addEvent(node, type, listener) {//
		if (!isCompatible()) {
			return false;
		}
		if (!(node = $(node))) {
			return false;
		}
		if (node.addEventListener) {
			node.addEventListener(type, listener, false);
			return true;
		}
		else if (node.attachEvent) {
			node['e' + type + listener] = listener;
			node[type + listener] = function () {
				node['e' + type + listener](window.event);
			}
			node.attachEvent('on' + type, node[type + listener]);
			return true;
		}
		return false;
	}
	window[packageName]['addEvent'] = addEvent;

	function removeEvent(node, type, listener) {//
		if (!(node = $(node))) {
			return false;
		}
		if (node.removeEventListener)//w3c的方法
		{
			node.removeEventListener(type, listener, false);
			return true;
		}
		else if (node.detachEvent) {
			node.detachEvent('on' + type, node[type + listener]);
			node[type + listener] = null;
			return true;
		}
		return false;
	}
	window[packageName]['removeEvent'] = removeEvent;

	function addLoadEvent(func) {
		var oldonload = window.onload;
		if (typeof window.onload != 'function') {
			window.onload = func;
		} else {
			window.onload = function () {
				oldonload();
				func();
			}
		}
	}
	window[packageName]['addLoadEvent'] = addLoadEvent;

	function hasClassName(classNameNow, searchName) {
		return new RegExp("( ?|^)" + searchName + "\\b").test(classNameNow);
	}
	window[packageName]['hasClassName'] = hasClassName;

	function addClass(element, value) {
		//alert(element);
		if (!element.className) {
			element.className = value;
		} else {
			newClassName = element.className;
			newClassName += " ";
			newClassName += value;
			element.className = newClassName;
		}
		//alert(element.innerHTML);
	}
	window[packageName]['addClass'] = addClass;

	function removeClass(element, searchName) {
		var classNameNow = element.className;
		element.className = classNameNow.replace(new RegExp("( ?|^)" + searchName + "\\b"), "");
	}
	window[packageName]['removeClass'] = removeClass;

	function show(obj, type) {
		if (type) {
			obj.style.display = type;
		}
		else {
			obj.style.display = 'block';
		}
	}
	window[packageName]['show'] = show;

	function hide(obj) {
		obj.style.display = 'none';
	}
	window[packageName]['hide'] = hide;

	Array.prototype.each = function (callback) {
		var len = this.length;
		for (var i = 0; i < len; i++) {
			callback.apply(this[i], arguments);
		}
	}

	/**
	* @param 父级节点
	* @param 需要筛选的子级节点的标签名,多个标签之间可以使用逗号隔开
	* @param 需要筛选的子级节点的className
	*/
	function getElementsByClassName(parentNode, subNodeName, className) {
		var elements = new Array();

		if (subNodeName.indexOf(',') == -1) {
			var children = parentNode.getElementsByTagName(subNodeName);
			var len = children.length;

			if (len > 0) {
				for (var i = 0; i < len; i++) {
					var child = children[i];

					if (hasClassName(child.className, className)) {
						elements.push(child);
					}
				}
			}
		}
		else {
			var tags = subNodeName.split(',');
			for (var i = 0, tagLen = tags.length; i < tagLen; i++) {
				var elementNodes = parentNode.getElementsByTagName(tags[i]);
				for (var j = 0, eleLen = elementNodes.length; j < eleLen; j++) {
					var child = elementNodes[j];
					if (hasClassName(child.className, className)) {
						elements.push(child);
					}
				}

			}
		}

		return elements;
	}
	window[packageName]['getElementsByClassName'] = getElementsByClassName;

	function using(functionName) {
		return window[packageName][functionName];
	}
	window[packageName]['using'] = using;

	function usingAll(functionStr, scope) {
		var scopeNow = scopeNow || window;
		var functionList = functionStr.split(',');
		for (var i = 0, len = functionList.length; i < len; i++) {
			var functionNow = functionList[i]
			if (functionNow) {
				scopeNow[functionNow] = window[packageName][functionNow];
			}
		}
	}
	window[packageName]['usingAll'] = usingAll;

	function sendRequest(method, url, content, callback, type, process, error) {
		http_request = false;
		if (window.XMLHttpRequest) {
			http_request = new XMLHttpRequest();
			if (http_request.overrideMimeType) {
				http_request.overrideMimeType("text/xml");
			}
		}
		else if (window.ActiveXObject) {
			try {
				http_request = new ActiveXObject(Msxml2.XMLHTTP);
			} catch (e) {
				try {
					http_request = new ActiveXObject("Microsoft.XMLHTTP");
				} catch (e) { }
			}
		}
		if (!http_request) {
			if (debugFlag) {
				window.alert("can't create XMLHttpRequest instance");
			}
			return false;
		}

		http_request.onreadystatechange = function () {
			if (http_request.readyState == 4) {

				if (http_request.status == 200 || http_request.status == 304) {
					var resp = http_request.responseText;
					if (type && type == 'json') {
						callback(typeof (resp) == 'object' ? resp : eval('(' + resp + ')'));
					} else {
						callback(resp);
					}
				} else {
					if (error) {
						error();
					}
				}
			} else {
				if (process) {
					process();
				}
			}
		};

		if (method == "get") {
			http_request.open(method, url, true);
		} else if (method == "post") {
			http_request.open(method, url, true);
			http_request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
		} else {
			if (debugFlag) {
				windows.alert("invalid type of http request!");
			}
			return false;
		}
		http_request.send(content);
	}
	window[packageName]['sendRequest'] = sendRequest;

	var log = {};
	log.info = function (msg) {
		if (typeof console != 'undefined' && console.info) {
			console.info.apply(console, arguments);
		}
	}
	log.warn = function (msg) {
		if (typeof console != 'undefined' && console.warn) {
			console.warn.apply(console, arguments);
		}
	}
	log.error = function (msg) {
		if (typeof console != 'undefined' && console.error) {
			console.error.apply(console, arguments);
		}
	}
	window[packageName]['log'] = log;
})();
